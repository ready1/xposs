/* global angular, document, window */
'use strict';

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $ionicPopover, $timeout) {
    // Form data for the login modal
    $scope.loginData = {};
    $scope.isExpanded = false;
    $scope.hasHeaderFabLeft = false;
    $scope.hasHeaderFabRight = false;

    var navIcons = document.getElementsByClassName('ion-navicon');
    for (var i = 0; i < navIcons.length; i++) {
        navIcons.addEventListener('click', function() {
            this.classList.toggle('active');
        });
    }

    ////////////////////////////////////////
    // Layout Methods
    ////////////////////////////////////////

    $scope.hideNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'none';
    };

    $scope.showNavBar = function() {
        document.getElementsByTagName('ion-nav-bar')[0].style.display = 'block';
    };

    $scope.noHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }
    };

    $scope.setExpanded = function(bool) {
        $scope.isExpanded = bool;
    };

    $scope.setHeaderFab = function(location) {
        var hasHeaderFabLeft = false;
        var hasHeaderFabRight = false;

        switch (location) {
            case 'left':
                hasHeaderFabLeft = true;
                break;
            case 'right':
                hasHeaderFabRight = true;
                break;
        }

        $scope.hasHeaderFabLeft = hasHeaderFabLeft;
        $scope.hasHeaderFabRight = hasHeaderFabRight;
    };

    $scope.hasHeader = function() {
        var content = document.getElementsByTagName('ion-content');
        for (var i = 0; i < content.length; i++) {
            if (!content[i].classList.contains('has-header')) {
                content[i].classList.toggle('has-header');
            }
        }

    };

    $scope.hideHeader = function() {
        $scope.hideNavBar();
        $scope.noHeader();
    };

    $scope.showHeader = function() {
        $scope.showNavBar();
        $scope.hasHeader();
    };

    $scope.clearFabs = function() {
        var fabs = document.getElementsByClassName('button-fab');
        if (fabs.length && fabs.length > 1) {
            fabs[0].remove();
        }
    };
})

.controller('LoginCtrl', function($scope, $timeout, $stateParams, ionicMaterialInk) {
    $scope.$parent.clearFabs();
    $timeout(function() {
        $scope.$parent.hideHeader();
    }, 0);
    ionicMaterialInk.displayEffect();
})

    .controller('DesignerSetupCtrl', function($scope, $timeout, $stateParams, ionicMaterialInk) {
        $scope.$parent.clearFabs();
        $timeout(function() {
            $scope.$parent.hideHeader();
        }, 0);

        ionicMaterialInk.displayEffect();
    })
    //.factory('instagram', ['$http',
    //    function($http) {
    //        return {
    //            fetchPopular: function(callback) {
    //
    //                //var endPoint = "https://api.instagram.com/v1/media/popular?client_id=1103601553.1677ed0.1c12653d3c904e1dba682f3e855860dc&callback=JSON_CALLBACK";
    //                var endPoint = "https://api.instagram.com/v1/users/yousef__akbar/media/recent/?access_token=1103601553.1677ed0.1c12653d3c904e1dba682f3e855860dc&callback=JSON_CALLBACK";
    //
    //                $http.jsonp(endPoint).success(function(response) {
    //                    callback(response.data);
    //                });
    //            }
    //        }
    //    }
    //])

    .factory('haven', ['$http',
        function($http) {
            return {
                fetchRecommendation: function(callback) {

                    var endPoint = "https://api.havenondemand.com/1/api/sync/recommend/v1?json=&url=https%3A%2F%2Fwww.havenondemand.com%2Fsample-content%2Fprediction%2F50KRecommend.json&service_name=50KServiceJSON&required_label=%3E50K&recommendations_amount=3&apikey=d5c02115-33b6-420d-a447-b95d7a9c83d3";

                    $http.jsonp(endPoint).success(function(response) {
                        callback(response.data);
                    });
                }
            }
        }
    ])
    .controller('DesignerHomeCtrl', function($scope, $timeout, $stateParams, ionicMaterialInk, ionicMaterialMotion) {
        $scope.$parent.clearFabs();

        // have a video id
        $scope.theBestVideo = 'DZBP3rxCigc';
        console.log($scope.theBestVideo);

        $timeout(function() {
            $scope.$parent.hideHeader();
        }, 0);

        // Set Motion
        $timeout(function() {
            ionicMaterialMotion.slideUp({
                selector: '.slide-up'
            });
        }, 300);

        //$timeout(function() {
        //    ionicMaterialMotion.fadeSlideInRight({
        //        startVelocity: 3000
        //    });
        //}, 700);

        ionicMaterialInk.displayEffect();
    })

    .controller('RetailerHomeCtrl', function($scope, $timeout, $stateParams, ionicMaterialInk, ionicMaterialMotion) {
        $scope.$parent.clearFabs();

        $timeout(function() {
            $scope.$parent.hideHeader();
        }, 0);

        // Set Motion
        $timeout(function() {
            ionicMaterialMotion.slideUp({
                selector: '.slide-up'
            });
        }, 300);

        //$timeout(function() {
        //    ionicMaterialMotion.fadeSlideInRight({
        //        startVelocity: 3000
        //    });
        //}, 700);

        ionicMaterialInk.displayEffect();

        $scope.initialize = function() {
            console.log(
                "Init Map"
            );

            var myLatlng = new google.maps.LatLng(-33.8819947,151.2190268);

            var mapOptions = {
                position: myLatlng,
                pov: {heading: 280, pitch: 0},
                zoom: 1
            };
            var map = new google.maps.StreetViewPanorama(document.getElementById("map-panorama"),
                mapOptions);


            $scope.map = map;
        };

        $timeout(function() {
            //google.maps.event.addDomListener(window, 'load', $scope.initialize);
            $scope.initialize();
        }, 0);
    })

    .controller('RetailerFindCtrl', function($compile, $state, $scope, $timeout, $stateParams, ionicMaterialInk, ionicMaterialMotion) {
        $scope.$parent.clearFabs();

        $timeout(function() {
            $scope.$parent.hideHeader();
        }, 0);

        // Set Motion
        //$timeout(function() {
        //    ionicMaterialMotion.slideUp({
        //        selector: '.slide-up'
        //    });
        //}, 300);

        ionicMaterialInk.displayEffect();

        $scope.initialize = function() {
            console.log(
                "Init Map"
            );

            var myLatlng = new google.maps.LatLng(-33.871719,151.206146);

            var mapOptions = {
                center: myLatlng,
                zoom: 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"),
                mapOptions);


            $scope.map = map;
        };

        $scope.showInfo = function(r){
            if($scope.infoWindow){
                $scope.infoWindow.close();
                $scope.infoWindow = null;
            }

            var compiled = $compile("<div><img src='"+ r.image+"' style='height: 80px' /><p></p><button class='button button-full button-raised button-balanced' ng-click='gotoRetailer()'> Connect</button></div>")($scope);

            var infoWindow = new google.maps.InfoWindow();
            infoWindow.setOptions({
                //content: "<div>"+ r.name +"</div>",
                content: compiled[0],
                position: new google.maps.LatLng(r.lat, r.lng),
            });

            infoWindow.open($scope.map);

            $scope.infoWindow = infoWindow;
        };

        $timeout(function() {
            //google.maps.event.addDomListener(window, 'load', $scope.initialize);
            $scope.initialize();
        }, 0);

        $scope.gotoRetailer = function(){
            $state.go("app.retailer-home");
        }

        $scope.scrollMap = function(r){
            $scope.map.setCenter(new google.maps.LatLng(r.lat, r.lng));
            $scope.showInfo(r);
            // app.retailer-home
            //$state.go("app.retailer-home");
        };

        $scope.retailersList = [
            {
                name: "Desordre",
                image:"../img/Desordre.png",
                lat: -33.885778,
                lng: 151.2259853
            }   ,
            {
                name: "Alice",
                image:"../img/alice.png",
                lat: -33.8723963	,
                lng: 151.2252006
            },
            {
                name: "ParlourX",
                image:"../img/ParlourX.png",
                lat: -33.823778,
                lng: 151.2259853
            },
            {
                name: "Husk",
                image:"../img/Husk.png",
                lat: -33.885773	,
                lng: 151.1931546
            }   ,
            {
                name: "Pet",
                image:"../img/Pet.png",
                lat: -33.895778,
                lng: 151.2259853
            }   ,
            {
                name: "Hansen",
                image:"../img/Hansen.png",
                lat: -33.884774	,
                lng: 151.2238003
            }   ,
        ];
    })

    .controller('DesignerSetupProfileCtrl', function($state, $scope, $timeout, $stateParams, ionicMaterialInk, $ionicModal) {
        $scope.$parent.clearFabs();
        $timeout(function() {
            $scope.$parent.hideHeader();
        }, 0);

        ionicMaterialInk.displayEffect();

        $scope.currentQuestion = 0;
        $scope.selectedCountry = {};
        $scope.selectedCategory = [];
        $scope.selectedPricePoint = {};
        $scope.selectedLook = [];
        $scope.retailersList = [];
        $scope.retailersListOrig = [
            {
                name: "Alice",
                image:"../img/alice.png"
            },
            {
                name: "Assin",
                image:"../img/assin.png"
            }   ,
            {
                name: "Burke Boutique",
                image:"../img/burke.png"
            }   ,
            {
                name: "Calexico",
                image:"../img/Calexico.png"
            },
            {
                name: "Bloodorange",
                image:"../img/Bloodorange.png"
            }   ,
            {
                name: "ParlourX",
                image:"../img/ParlourX.png"
            },
            {
                name: "Pet",
                image:"../img/Pet.png"
            }   ,
            {
                name: "Concrete",
                image:"../img/Concrete.png"
            }   ,
            {
                name: "Desordre",
                image:"../img/Desordre.png"
            }   ,
            {
                name: "Husk",
                image:"../img/Husk.png"
            }   ,
            {
                name: "Incu",
                image:"../img/Incu.png"
            } ,
            {
                name: "Mary",
                image:"../img/Mary.png"
            }
        ];

        $scope.retailersListOrig2 = [
            {
                name: "Alice",
                image:"../img/alice.png"
            },
            {
                name: "Burke Boutique",
                image:"../img/burke.png"
            }   ,
            {
                name: "Calexico",
                image:"../img/Calexico.png"
            },
            {
                name: "ParlourX",
                image:"../img/ParlourX.png"
            },
            {
                name: "Pet",
                image:"../img/Pet.png"
            }   ,
            {
                name: "Concrete",
                image:"../img/Concrete.png"
            },
            {
                name: "Bloodorange",
                image:"../img/Bloodorange.png"
            }   ,
        ];

        $scope.retailersListOrig3 = [
            {
                name: "Desordre",
                image:"../img/Desordre.png"
            }   ,
            {
                name: "Alice",
                image:"../img/alice.png"
            },
            {
                name: "ParlourX",
                image:"../img/ParlourX.png"
            },
            {
                name: "Husk",
                image:"../img/Husk.png"
            }   ,
            {
                name: "Pet",
                image:"../img/Pet.png"
            }   ,
        ];

        $scope.countries = [
            {
                name: "Australia",
                image:"../img/australia.png"
            },
            {
                name: "France",
                image:"../img/france.png"
            },
            {
                name: "USA",
                image:"../img/usa.png"
            },
            {
                name: "London",
                image:"../img/london.png"
            },
            {
                name: "China",
                image:"../img/china.png"
            }
        ];

        $scope.categories = [
            {
                name: "Men's",
                image:"../img/mens.png"
            },
            {
                name: "Women's",
                image:"../img/womens.png"
            },
            {
                name: "Apparel",
                image:"../img/apparel.png"
            },
            {
                name: "Accessories",
                image:"../img/acessories.png"
            },
            {
                name: "Footwear",
                image:"../img/Footwear.png"
            },
            {
                name: "Jewellery",
                image:"../img/Jewellery.png"
            },
            {
                name: "Intimates",
                image:"../img/Intimates.png"
            },
            {
                name: "Lingerie",
                image:"../img/Lingerie.png"
            },
            {
                name: "Swim",
                image:"../img/Swim.png"
            }
        ];

        $scope.pricepoints = [
            {
                name: "Affordable (< $100)",
                image:"../img/affordable.png"
            },
            {
                name: "Moderate ($100-$300)",
                image:"../img/moderate.png"
            },
            {
                name: "Better ($300-$600)",
                image:"../img/better.png"
            },
            {
                name: "Luxury ($600+)",
                image:"../img/luxury.png"
            }
        ];

        $scope.styles = [
            {
                name: "Activewear",
                image:"../img/Activewear.png"
            },
            {
                name: "Bodycon",
                image:"../img/Bodycon.png"
            },
            {
                name: "Boho",
                image:"../img/Boho.png"
            },
            {
                name: "Boxy",
                image:"../img/Boxy.png"
            },
            {
                name: "Business",
                image:"../img/Business.png"
            },
            {
                name: "Casual",
                image:"../img/Casual.png"
            },
            {
                name: "Chic",
                image:"../img/Chic.png"
            },
            {
                name: "Classic",
                image:"../img/Classic.png"
            },
            {
                name: "Denim",
                image:"../img/Denim.png"
            },
            {
                name: "Evening",
                image:"../img/Evening.png"
            },
            {
                name: "Feminine",
                image:"../img/Feminine.png"
            },
            {
                name: "Flamboyant",
                image:"../img/Flamboyant.png"
            },
            {
                name: "Formal",
                image:"../img/Formal.png"
            },
            {
                name: "Glam",
                image:"../img/Glam.png"
            },
            {
                name: "Knits",
                image:"../img/Knits.png"
            },
            {
                name: "Minimal",
                image:"../img/Minimal.png"
            }
        ];

        $scope.getRecommendedRetailers = function(){
            // utilise havenon demand
            // for it Prediction > Recommendation api
            // but its not working at the moment
        }

        $ionicModal.fromTemplateUrl('my-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };

        $scope.completeRegistration = function() {
            $scope.modal.hide();
            $state.go("app.designer-home");
        };

        $scope.gotoQuestion = function(q){
            $scope.currentQuestion = q;
        };

        $scope.answerQ1 = function(country){
            $scope.selectedCountry = country;
            $scope.currentQuestion = 1;

            $scope.retailersList = $scope.retailersListOrig;
        };

        $scope.answerQ2 = function(category){
            //$scope.currentQuestion = 2;
            var exists = false;
            for(var key in $scope.selectedCategory){
                var cat = $scope.selectedCategory[key];
                exists = cat.name === category.name;
            }

            if(!exists){
                $scope.selectedCategory.push(category);
            }

            $scope.retailersList = $scope.retailersListOrig2;

        };

        $scope.answerQ3 = function(pricepoint){
            $scope.selectedPricePoint = pricepoint;
            $scope.currentQuestion = 3;
        };

        $scope.answerQ4 = function(style){
            //$scope.currentQuestion = 0;
            var exists = false;
            for(var key in $scope.selectedLook){
                var cat = $scope.selectedLook[key];
                exists = cat.name === style.name;
            }

            if(!exists){
                $scope.selectedLook.push(style);
            }

            $scope.retailersList = $scope.retailersListOrig3;
        };
    })



.controller('FriendsCtrl', function($scope, $stateParams, $timeout, ionicMaterialInk, ionicMaterialMotion) {
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.$parent.setHeaderFab('left');

    // Delay expansion
    $timeout(function() {
        $scope.isExpanded = true;
        $scope.$parent.setExpanded(true);
    }, 300);

    // Set Motion
    ionicMaterialMotion.fadeSlideInRight();

    // Set Ink
    ionicMaterialInk.displayEffect();
})

.controller('ProfileCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    // Set Header
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = false;
    $scope.$parent.setExpanded(false);
    $scope.$parent.setHeaderFab(false);

    // Set Motion
    $timeout(function() {
        ionicMaterialMotion.slideUp({
            selector: '.slide-up'
        });
    }, 300);

    $timeout(function() {
        ionicMaterialMotion.fadeSlideInRight({
            startVelocity: 3000
        });
    }, 700);

    // Set Ink
    ionicMaterialInk.displayEffect();
})

.controller('ActivityCtrl', function($scope, $stateParams, $timeout, ionicMaterialMotion, ionicMaterialInk) {
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = true;
    $scope.$parent.setExpanded(true);
    $scope.$parent.setHeaderFab('right');

    $timeout(function() {
        ionicMaterialMotion.fadeSlideIn({
            selector: '.animate-fade-slide-in .item'
        });
    }, 200);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();
})

.controller('GalleryCtrl', function($scope, $stateParams, $timeout, ionicMaterialInk, ionicMaterialMotion) {
    $scope.$parent.showHeader();
    $scope.$parent.clearFabs();
    $scope.isExpanded = true;
    $scope.$parent.setExpanded(true);
    $scope.$parent.setHeaderFab(false);

    // Activate ink for controller
    ionicMaterialInk.displayEffect();

    ionicMaterialMotion.pushDown({
        selector: '.push-down'
    });
    ionicMaterialMotion.fadeSlideInRight({
        selector: '.animate-fade-slide-in .item'
    });

})

;
